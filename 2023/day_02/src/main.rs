use std::str::FromStr;

const MAX_RED_CUBES: u32 = 12;
const MAX_GREEN_CUBES: u32 = 13;
const MAX_BLUE_CUBES: u32 = 14;

fn main() {
    let input = include_str!("../input.txt");
    println!("--- Day 2: Cube Conundrum ---");
    println!("Part 1 answer: {}", solve_part_1(input));
    println!("Part 2 answer: {}", solve_part_2(input));
}

#[derive(Debug)]
struct Game {
    id: u32,
    cube_subsets: Vec<CubeSubset>,
}

impl Game {
    fn is_valid(&self) -> bool {
        for cube_subset in &self.cube_subsets {
            if cube_subset.red_cubes > MAX_RED_CUBES {
                return false;
            }
            if cube_subset.green_cubes > MAX_GREEN_CUBES {
                return false;
            }
            if cube_subset.blue_cubes > MAX_BLUE_CUBES {
                return false;
            }
        }
        return true;
    }
}

impl FromStr for Game {
    /**
     * Receive a string looking like this:
     * Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
     */
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let game_id_and_subsets_split: Vec<&str> = s.split(": ").collect();

        // Capture game ID
        // Receive a string looking like this:
        // Game 1
        let game_id: u32 = game_id_and_subsets_split[0]
            .split(" ")
            .collect::<Vec<&str>>()[1]
            .parse()
            .expect("Game ID should be a number");

        // Capture cube subsets for this game
        // Receive a string looking like this:
        // 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        let cube_subsets: Vec<CubeSubset> = game_id_and_subsets_split[1]
            .split("; ")
            .map(|cs| {
                cs.parse()
                    .expect("CubeSubset should have been able to be parsed")
            })
            .collect();

        let game = Self {
            id: game_id,
            cube_subsets,
        };

        Ok(game)
    }
}

// impl parse
#[derive(Debug, Default)]
struct CubeSubset {
    red_cubes: u32,
    green_cubes: u32,
    blue_cubes: u32,
}

impl FromStr for CubeSubset {
    /**
     * Receive a string looking like this:
     * 8 green, 6 blue, 20 red
     */
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let cube_color_sets: Vec<&str> = s.split(", ").collect();
        let mut cube_subset = CubeSubset::default();

        for ccs in cube_color_sets {
            let count_color_split: Vec<&str> = ccs.split(" ").collect();
            let count: u32 = count_color_split[0]
                .parse()
                .expect("Count of colored cube should be a number");

            match count_color_split[1] {
                "red" => cube_subset.red_cubes = count,
                "green" => cube_subset.green_cubes = count,
                "blue" => cube_subset.blue_cubes = count,
                _ => panic!("Received an unknown color: {}", count_color_split[1]),
            }
        }

        Ok(cube_subset)
    }
}

fn solve_part_1(input: &str) -> u32 {
    // parse input into data
    input
        .lines()
        .map(|line| {
            let game: Game = line.parse().expect(&format!(
                "Should have been able to parse cube_subset: {}",
                line
            ));
            game
        })
        .filter(|game| game.is_valid())
        .map(|game| game.id)
        .sum()
}

fn solve_part_2(input: &str) -> u32 {
    let games: Vec<Game> = input
        .lines()
        .map(|line| {
            let game: Game = line.parse().expect(&format!(
                "Should have been able to parse cube_subset: {}",
                line
            ));
            game
        })
        .collect();

    let game_cube_powers: u32 = games
        .iter()
        .map(|game| {
            let mut fewest_possible_cubes = CubeSubset::default();

            for cube_subset in &game.cube_subsets {
                if fewest_possible_cubes.red_cubes < cube_subset.red_cubes {
                    fewest_possible_cubes.red_cubes = cube_subset.red_cubes;
                }
                if fewest_possible_cubes.green_cubes < cube_subset.green_cubes {
                    fewest_possible_cubes.green_cubes = cube_subset.green_cubes;
                }
                if fewest_possible_cubes.blue_cubes < cube_subset.blue_cubes {
                    fewest_possible_cubes.blue_cubes = cube_subset.blue_cubes;
                }
            }

            let cube_power = fewest_possible_cubes.red_cubes
                * fewest_possible_cubes.green_cubes
                * fewest_possible_cubes.blue_cubes;
            return cube_power;
        })
        .sum();

    game_cube_powers
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1_example() {
        let input = include_str!("../input_example_1.txt");
        assert_eq!(solve_part_1(input), 8);
    }

    #[test]
    fn test_part_2_example() {
        let input = include_str!("../input_example_2.txt");
        assert_eq!(solve_part_2(input), 2286);
    }
}
