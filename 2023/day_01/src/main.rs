fn main() {
    let input = include_str!("../input.txt");
    println!("--- Day 1: Trebuchet?! ---");
    println!("Part 1 answer: {}", solve_part_1(input));
    println!("Part 2 answer: {}", solve_part_2(input));
}

fn solve_part_1(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            let digits: Vec<char> = line.chars().filter(|c| c.is_ascii_digit()).collect();

            let first_digit = digits.first().unwrap();
            let last_digit = digits.last().unwrap();

            let number = format!("{}{}", first_digit, last_digit);
            let number: u32 = number
                .parse()
                .expect("Should be able to parse a two digit number");
            number
        })
        .sum()
}

fn solve_part_2(input: &str) -> u32 {
    let spelled_out_digits = vec![
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];

    input
        .lines()
        .map(|line| {
            let mut digits: Vec<char> = Vec::new();
            let mut temp = String::new();

            for ch in line.chars() {
                // Always push raw digits
                if ch.is_ascii_digit() {
                    digits.push(ch);
                    // Clear out temp
                    temp = String::new();
                    continue;
                }

                temp.push(ch);

                for (i, spelled_digit) in spelled_out_digits.iter().enumerate() {
                    if temp.contains(spelled_digit) {
                        let number = i + 1;
                        digits.push(
                            number
                                .to_string()
                                .chars()
                                .next()
                                .expect("number should be at least one character long"),
                        );

                        // Reset temp to the currenct character - some numbers overlap
                        temp = ch.to_string();
                    }
                }
            }

            let first_digit = digits.first().unwrap();
            let last_digit = digits.last().unwrap();

            let number = format!("{}{}", first_digit, last_digit);
            let number: u32 = number
                .parse()
                .expect("Digits vector should have been only digit characters!");

            number
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1_example() {
        let input = include_str!("../input_example_1.txt");
        assert_eq!(solve_part_1(input), 142);
    }

    #[test]
    fn test_part_2_example() {
        let input = include_str!("../input_example_2.txt");
        assert_eq!(solve_part_2(input), 281);
    }
}
