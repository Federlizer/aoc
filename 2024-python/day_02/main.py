INCREASING = "increasing"
DECREASING = "decreasing"
INVALID = "invalid"


def parse_input(input):
    reports = []
    for line in input.split("\n"):
        if not line:
            continue
        numbers = [int(n) for n in line.split(" ")]
        reports.append(numbers)
    return reports


def solve_part_1(input):
    safe_reports = []
    all_reports = parse_input(input)
    for report in all_reports:
        if is_safe(report):
            safe_reports.append(report)
    return len(safe_reports)


def is_safe(report):
    increment_type = check_increment(report)
    if increment_type == INVALID:
        return False
    if not check_adjecent_levels(report):
        return False
    return True


def check_increment(report):
    # Check for decrement
    decreasing = True
    increasing = True
    for i, level in enumerate(report):
        if i == 0:
            continue
        if level > report[i-1]:
            decreasing = False
            break  # Not a decreasing report
    # Check for increment
    for i, level in enumerate(report):
        if i == 0:
            continue
        if level < report[i-1]:
            increasing = False
            break  # Not an increasing report
    # Return value
    if increasing:
        return INCREASING
    elif decreasing:
        return DECREASING
    else:
        return INVALID


def check_adjecent_levels(report):
    min_difference = 1
    max_difference = 3
    for i, level in enumerate(report):
        if i == 0:
            continue
        difference = abs(level - report[i-1])
        if difference > max_difference or difference < min_difference:
            return False
    return True


def solve_part_2(input):
    """Problem dampner removes only a single level."""
    safe_reports = []
    all_reports = parse_input(input)
    for report in all_reports:
        if is_safe(report):
            safe_reports.append(report)
        else:
            # Try same report by engaging problem dampner
            for i in range(len(report)):
                # Remove that index (`i`)
                report_clone = report[0:i] + report[i+1:len(report)]
                if is_safe(report_clone):
                    safe_reports.append(report)
                    break
    return len(safe_reports)


if __name__ == "__main__":
    with open("./input.txt", "r") as input_fh:
        input = input_fh.read()
        safe_reports = solve_part_1(input)
        print(f"Safe reports: {safe_reports}")
        safe_reports_with_problem_dampner = solve_part_2(input)
        print(f"Safe reports with problem dampner: {safe_reports_with_problem_dampner}")
