import unittest

from main import solve_part_1, solve_part_2


class TestPartOneSolution(unittest.TestCase):

    def test_part_one(self):
        input = """7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9"""
        output = solve_part_1(input)
        self.assertEqual(output, 2)

    def test_part_two(self):
        input = """7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9"""
        output = solve_part_2(input)
        self.assertEqual(output, 4)
