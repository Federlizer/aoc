import sys
import time

EMPTY_SPACE_SYMBOL = "."
OBSTRUCTION_SYMBOL = "#"
# Guard symbols
GUARD_FACING_UP_SYMBOL = "^"
GUARD_FACING_DOWN_SYMBOL = "v"
GUARD_FACING_LEFT_SYMBOL = "<"
GUARD_FACING_RIGHT_SYMBOL = ">"

GUARD_SYMBOLS = [
    GUARD_FACING_UP_SYMBOL,
    GUARD_FACING_DOWN_SYMBOL,
    GUARD_FACING_LEFT_SYMBOL,
    GUARD_FACING_RIGHT_SYMBOL,
]

GUARD_OUT_OF_BOUNDS = "out_of_bounds"
GUARD_IN_LOOP = "guard_in_loop"


class Position(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"({self.x}, {self.y})"

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


class GuardPosition(Position):

    def __init__(self, x, y, direction):
        super().__init__(x, y)
        self.direction = direction

    def __repr__(self):
        return f"(x {self.x}, y {self.y}) - {self.direction}"

    def __eq__(self, other):
        if not isinstance(other, GuardPosition):
            return super().__eq__(other)
        return (self.x == other.x and
                self.y == other.y and
                self.direction == other.direction)


class ObstructionError(ValueError):
    pass


class MapCell(object):

    EMPTY_SPACE_CELL = "empty_space"
    OBSTRUCTION_CELL = "obstruction"
    # Guard cells
    GUARD_FACING_UP_CELL = "guard_facing_up"
    GUARD_FACING_DOWN_CELL = "guard_facing_down"
    GUARD_FACING_LEFT_CELL = "guard_facing_left"
    GUARD_FACING_RIGHT_CELL = "guard_facing_right"

    GUARD_CELLS = [
        GUARD_FACING_UP_CELL,
        GUARD_FACING_DOWN_CELL,
        GUARD_FACING_LEFT_CELL,
        GUARD_FACING_RIGHT_CELL,
    ]

    CELL_TYPE_TO_SYMBOL_MAP = {
        EMPTY_SPACE_CELL: EMPTY_SPACE_SYMBOL,
        OBSTRUCTION_CELL: OBSTRUCTION_SYMBOL,
        # Guard cells
        GUARD_FACING_UP_CELL: GUARD_FACING_UP_SYMBOL,
        GUARD_FACING_DOWN_CELL: GUARD_FACING_DOWN_SYMBOL,
        GUARD_FACING_LEFT_CELL: GUARD_FACING_LEFT_SYMBOL,
        GUARD_FACING_RIGHT_CELL: GUARD_FACING_RIGHT_SYMBOL,
    }

    SYMBOL_TO_CELL_TYPE_MAP = {
        EMPTY_SPACE_SYMBOL: EMPTY_SPACE_CELL,
        OBSTRUCTION_SYMBOL: OBSTRUCTION_CELL,
        # Guard cells
        GUARD_FACING_UP_SYMBOL: GUARD_FACING_UP_CELL,
        GUARD_FACING_DOWN_SYMBOL: GUARD_FACING_DOWN_CELL,
        GUARD_FACING_LEFT_SYMBOL: GUARD_FACING_LEFT_CELL,
        GUARD_FACING_RIGHT_SYMBOL: GUARD_FACING_RIGHT_CELL,
    }

    def __init__(self, cell_type):
        self.cell_type = cell_type
        self.visited_count = 0
        # The amount of times that a guard has visited this cell
        if cell_type in MapCell.GUARD_CELLS:
            self.visited_count = 1

    def __eq__(self, other):
        if isinstance(other, MapCell):
            return (self.cell_type == other.cell_type and
                    self.visited_count == other.visited_count)
        elif isinstance(other, str):
            return self.cell_type == other
        else:
            raise TypeError(f"Cannot compare MapCell with {type(other)}")

    def get_symbol(self):
        return MapCell.CELL_TYPE_TO_SYMBOL_MAP[self.cell_type]

    def set_cell_type(self, new_cell_type):
        self.cell_type = new_cell_type
        if new_cell_type in MapCell.GUARD_CELLS:
            self.visited_count += 1


class Map(object):

    def __init__(self, input, extra_obstruction=None, stdscr=None):
        self._map = []
        self._initial_guard_position = None
        self._current_guard_position = None
        self._stdscr = stdscr
        # Populate map
        for y, row in enumerate(input.split("\n")):
            if not row:
                continue
            map_row = []
            for x, col in enumerate(row):
                # Find the position of the guard
                if col in GUARD_SYMBOLS:
                    # col is the exact symbol, hence also direction
                    guard_position = GuardPosition(x, y, col)
                    self._initial_guard_position = guard_position
                    self._current_guard_position = guard_position
                cell_type = MapCell.SYMBOL_TO_CELL_TYPE_MAP[col]
                cell = MapCell(cell_type)
                map_row.append(cell)
            self._map.append(map_row)
        # Add extra obstruction if it has been passed
        if extra_obstruction:
            extra_obstruction_pos = Position(extra_obstruction[0],
                                             extra_obstruction[1])
            if self._is_out_of_bounds(extra_obstruction_pos):
                raise ObstructionError("Cannot place obstruction out of "
                                       "bounds!")
            if extra_obstruction_pos == self._initial_guard_position:
                raise ObstructionError("Extra obstruction cannot be the same "
                                       "as initial guard position.")
            if self._get_cell(extra_obstruction_pos) == MapCell.OBSTRUCTION_CELL:
                raise ObstructionError("Extra obstruction is placed on top of "
                                       "an existing obstruction.")
            self._set_cell(extra_obstruction_pos, MapCell.OBSTRUCTION_CELL)

    def __repr__(self):
        repr = ""
        for row in self._map:
            for cell in row:
                symbol = cell.get_symbol()
                if symbol == EMPTY_SPACE_SYMBOL and cell.visited_count > 0:
                    repr += "X"
                else:
                    repr += symbol
            repr += "\n"
        return repr

    def _get_cell(self, position):
        """Return the cell from the map given a position."""
        return self._map[position.y][position.x]

    def _set_cell(self, position, cell_type):
        """Set a cell on the map to a cell type."""
        self._map[position.y][position.x].set_cell_type(cell_type)

    def _get_guard_speed(self):
        """Get the guard's current speed - (x speed, y speed)."""
        if self._current_guard_position.direction == GUARD_FACING_UP_SYMBOL:
            return (0, -1)
        elif self._current_guard_position.direction == GUARD_FACING_DOWN_SYMBOL:
            return (0, +1)
        elif self._current_guard_position.direction == GUARD_FACING_LEFT_SYMBOL:
            return (-1, 0)
        elif self._current_guard_position.direction == GUARD_FACING_RIGHT_SYMBOL:
            return (+1, 0)
        else:
            raise ValueError("Unknown guard direction "
                             f"'{self._current_guard_position.direction}'")

    def _rotate_guard_right(self):
        """Rotate guard to the right."""
        # Up -> Right
        if self._current_guard_position.direction == GUARD_FACING_UP_SYMBOL:
            self._current_guard_position.direction = GUARD_FACING_RIGHT_SYMBOL
            self._set_cell(self._current_guard_position, MapCell.GUARD_FACING_RIGHT_CELL)
        # Right -> Down
        elif self._current_guard_position.direction == GUARD_FACING_RIGHT_SYMBOL:
            self._current_guard_position.direction = GUARD_FACING_DOWN_SYMBOL
            self._set_cell(self._current_guard_position, MapCell.GUARD_FACING_DOWN_CELL)
        # Down -> Left
        elif self._current_guard_position.direction == GUARD_FACING_DOWN_SYMBOL:
            self._current_guard_position.direction = GUARD_FACING_LEFT_SYMBOL
            self._set_cell(self._current_guard_position, MapCell.GUARD_FACING_LEFT_CELL)
        # Left -> Up
        elif self._current_guard_position.direction == GUARD_FACING_LEFT_SYMBOL:
            self._current_guard_position.direction = GUARD_FACING_UP_SYMBOL
            self._set_cell(self._current_guard_position, MapCell.GUARD_FACING_UP_CELL)
        # Unknown
        else:
            raise ValueError("Unknown guard direction "
                             f"'{self._current_guard_position.direction}'")

    def _is_out_of_bounds(self, position):
        """Check if position is out of bounds of the map's dimentions."""
        min_x = 0
        max_x = len(self._map[0]) - 1
        min_y = 0
        max_y = len(self._map) - 1
        if position.x < min_x or position.y < min_y:
            return True
        if position.x > max_x or position.y > max_y:
            return True

    def process(self):
        """
        If there is something directly in front of you, turn right 90 degrees.
        Otherwise, take a step forward.

        Until:
            - Guard is out of bounds
            - Guard is in the initial position and direction
        """

        while True:
            if self._stdscr:
                self._stdscr.clear()
                self._stdscr.addstr(f"{self}")
                self._stdscr.refresh()
                time.sleep(.2)
            # Get guard directional speed
            guard_speed_x, guard_speed_y = self._get_guard_speed()
            # Predict guard's next step
            next_guard_position = GuardPosition(
                self._current_guard_position.x + guard_speed_x,
                self._current_guard_position.y + guard_speed_y,
                self._current_guard_position.direction,
            )
            # If the guard is going out of bounds, get guard out and return
            if self._is_out_of_bounds(next_guard_position):
                self._set_cell(self._current_guard_position,
                               MapCell.EMPTY_SPACE_CELL)
                self._current_guard_position = next_guard_position
                return GUARD_OUT_OF_BOUNDS
            # If guard would visit this space for the 11th time, consider her
            # stuck in a loop
            if self._get_cell(next_guard_position).visited_count >= 10:
                return GUARD_IN_LOOP
            # Else, process guard's pathing rules
            # If guard is facing an obstruction - turn right
            if self._get_cell(next_guard_position) == MapCell.OBSTRUCTION_CELL:
                self._rotate_guard_right()
            # Otherwise, step forward
            else:
                # Set new cell to guard
                self._set_cell(next_guard_position,
                               MapCell.SYMBOL_TO_CELL_TYPE_MAP[
                                   self._current_guard_position.direction])
                # Set old cell to empty space again
                self._set_cell(self._current_guard_position,
                               MapCell.EMPTY_SPACE_CELL)
                # Update guard's position
                self._current_guard_position = next_guard_position

    def count_distinct_visited_spaces(self):
        visited_spaces = 0
        for row in self._map:
            for cell in row:
                if cell.visited_count > 0:
                    visited_spaces += 1
        return visited_spaces


def solve_part_1(input):
    map = Map(input)
    reason = map.process()
    if reason != GUARD_OUT_OF_BOUNDS:
        raise ValueError("Did not expect guard to be looping in part 1!")
    return map.count_distinct_visited_spaces()


def solve_part_2(input):
    rows = len(input.split("\n"))
    cols = len(input.split("\n")[0])
    total_looping_obstacles = 0
    for y in range(rows):
        for x in range(cols):
            try:
                map = Map(input, extra_obstruction=(x, y))
            except ObstructionError:
                # Cannot place obstruction here - guard or other
                # obstruction is here already
                continue
            reason = map.process()
            if reason == GUARD_IN_LOOP:
                total_looping_obstacles += 1
                print(f"Found new loop obsticle, new total: {total_looping_obstacles}")
    return total_looping_obstacles


if __name__ == "__main__":
    with open("./input.txt", "r") as input_fh:
        input = input_fh.read()
        result = solve_part_1(input)
        print(f"Part 1: {result}")
        result = solve_part_2(input)
        print(f"Part 2: {result}")
