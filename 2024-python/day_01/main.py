

def parse_input(input):
    left_column = []
    right_column = []
    for line in input.split("\n"):
        line = line.strip()
        if not line:  # Skip empty lines
            continue
        left_num, right_num = line.split("   ")
        left_column.append(int(left_num))
        right_column.append(int(right_num))
    return left_column, right_column


def solve_part_1(input):
    left_column, right_column = parse_input(input)
    # Sort the arrays
    left_column.sort()
    right_column.sort()
    total_distance = 0
    # Zip the arrays
    for pair in zip(left_column, right_column):
        distance = abs(pair[0] - pair[1])
        total_distance += distance
    return total_distance


def solve_part_2(input):
    left_column, right_column = parse_input(input)
    similarity_score = 0
    for location_id in left_column:
        if location_id not in right_column:
            continue
        similarity_score += location_id * right_column.count(location_id)
    return similarity_score


if __name__ == "__main__":
    with open("./input.txt", "r") as input_fh:
        input = input_fh.read()
        total_distance = solve_part_1(input)
        similarity_score = solve_part_2(input)
        print(f"Total distance: {total_distance}")
        print(f"Similarity score: {similarity_score}")
