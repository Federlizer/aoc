import unittest

from main import solve_part_1, solve_part_2


class TestPartOneSolution(unittest.TestCase):

    def test_solution(self):
        """Test with the example input."""
        input = """3   4
        4   3
        2   5
        1   3
        3   9
        3   3"""
        output = solve_part_1(input)
        self.assertEqual(output, 11)


class TestPartTwoSolution(unittest.TestCase):

    def test_solution(self):
        """Test with the example input."""
        input = """3   4
        4   3
        2   5
        1   3
        3   9
        3   3"""
        output = solve_part_2(input)
        self.assertEqual(output, 31)
