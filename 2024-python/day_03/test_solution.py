import unittest

from main import solve_part_1, solve_part_2


class TestSolutions(unittest.TestCase):

    def test_solution_part_1(self):
        input = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"
        expected_output = 161
        output = solve_part_1(input)
        self.assertEqual(output, expected_output)

    def test_solution_part_1_real_input(self):
        with open("./input.txt", "r") as input_fh:
            input = input_fh.read()
            expected_output = 191183308
            output = solve_part_1(input)
            self.assertEqual(output, expected_output)

    def test_solution_part_2(self):
        input = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"
        expected_output = 48
        output = solve_part_2(input)
        self.assertEqual(output, expected_output)

    def test_solution_part_2_real_input(self):
        with open("./input.txt", "r") as input_fh:
            input = input_fh.read()
            expected_output = "federlizer"
            output = solve_part_2(input)
            self.assertEqual(output, expected_output)
