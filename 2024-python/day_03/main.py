import re


def solve_part_1(input):
    mul_operator_pattern = re.compile(r"mul\((?P<left_num>[0-9]{1,3}),(?P<right_num>[0-9]{1,3})\)")
    total = 0
    for match in mul_operator_pattern.finditer(input):
        left_num = int(match.group("left_num"))
        right_num = int(match.group("right_num"))
        product = left_num * right_num
        total += product
    return total


OPERATOR_MUL = "mul"
OPERATOR_DO = "do"
OPERATOR_DO_NOT = "don't"


def solve_part_2(input):
    mul_operator_pattern = re.compile(r"(?P<operator>mul|do|don't)\(((?P<left_num>[0-9]{1,3}),(?P<right_num>[0-9]{1,3}))?\)")
    total = 0
    should_do = True  # Always on at start
    for match in mul_operator_pattern.finditer(input):
        operator = match.group("operator")
        # Process mul operators
        if operator == OPERATOR_MUL:
            left_num = int(match.group("left_num"))
            right_num = int(match.group("right_num"))
            if should_do:
                product = left_num * right_num
                total += product
        # Enable multiplication
        elif operator == OPERATOR_DO:
            should_do = True
        # Disable multiplication
        elif operator == OPERATOR_DO_NOT:
            should_do = False
        else:
            raise Exception(f"Unknown operator '{operator}'")

    return total


if __name__ == "__main__":
    with open("./input.txt", "r") as input_fh:
        input = input_fh.read()
        result = solve_part_1(input)
        print("Result of all multiplications: {}".format(result))
        more_accurate_result = solve_part_2(input)
        print(f"More accurate result: {more_accurate_result}")
