
class Position(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"<Position (x: {self.x}, y: {self.y})>"


class Puzzle(object):
    def __init__(self, input):
        self.puzzle = []
        for line in input.split("\n"):
            if not line:
                continue
            puzzle_line = []
            for ch in line:
                puzzle_line.append(ch)
            self.puzzle.append(puzzle_line)

    def __getitem__(self, key):
        if not isinstance(key, Position):
            raise TypeError(f"Cannot index with '{type(key)}'")
        # print(f"Trying to get item y={key.y}, x={key.x}")
        return self.puzzle[key.y][key.x]

    def __repr__(self):
        repr = ""
        for puzzle_line in self.puzzle:
            for ch in puzzle_line:
                repr += ch
            repr += "\n"
        return repr

    def print_relevant_positions(self, relevant_positions):
        # Print relevant characters
        relevant_positions = [(pos.x, pos.y) for pos in relevant_positions]
        for y, row in enumerate(self.puzzle):
            for x, col in enumerate(row):
                if (x, y) in relevant_positions:
                    print(col, end="")
                else:
                    print(".", end="")
            print("")


def search(puzzle, start_position, direction_x, direction_y):
    start_x = start_position.x
    start_y = start_position.y

    # Check if we have space
    max_x = start_x + direction_x * 3
    max_y = start_y + direction_y * 3

    if max_x < 0 or max_x > len(puzzle.puzzle[start_y]) - 1:
        return None
    if max_y < 0 or max_y > len(puzzle.puzzle) - 1:
        return None

    # Construct positions
    x_pos = Position(start_x,                 start_y)
    m_pos = Position(start_x + direction_x,   start_y + direction_y)
    a_pos = Position(start_x + direction_x*2, start_y + direction_y*2)
    s_pos = Position(start_x + direction_x*3, start_y + direction_y*3)

    # Check positions
    if puzzle[x_pos] == "X" and puzzle[m_pos] == "M" and puzzle[a_pos] == "A" and puzzle[s_pos] == "S":
        return (x_pos, m_pos, a_pos, s_pos)
    return None


def solve_part_1(input):
    relevant_positions = []
    puzzle = Puzzle(input)
    print(puzzle)

    xmass_found = 0
    for y, puzzle_line in enumerate(puzzle.puzzle):
        for x, ch in enumerate(puzzle_line):
            if ch == "X":
                pos = Position(x, y)

                # Search to the right
                positions_found = search(puzzle, pos, +1, 0)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1

                # Search to the left
                positions_found = search(puzzle, pos, -1, 0)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1

                # Search down
                positions_found = search(puzzle, pos, 0, +1)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1

                # Search up
                positions_found = search(puzzle, pos, 0, -1)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1

                # Search diagonals
                # up-right
                positions_found = search(puzzle, pos, +1, -1)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1
                # up-left
                positions_found = search(puzzle, pos, -1, -1)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1
                # down-right
                positions_found = search(puzzle, pos, +1, +1)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1
                # down-left
                positions_found = search(puzzle, pos, -1, +1)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1

    print(puzzle.print_relevant_positions(relevant_positions))
    return xmass_found


def search_v2(puzzle, start_position):
    start_x = start_position.x
    start_y = start_position.y

    # Check if we have space
    if start_y - 1 < 0 or start_x - 1 < 0:
        return None
    if start_y + 1 > len(puzzle.puzzle) - 1 or start_x + 1 > len(puzzle.puzzle[start_y]) - 1:
        return None

    # Construct positions
    center = Position(start_x, start_y)
    top_left = Position(start_x - 1, start_y - 1)
    top_right = Position(start_x + 1, start_y - 1)
    bot_left = Position(start_x - 1, start_y + 1)
    bot_right = Position(start_x + 1, start_y + 1)

    # Check positions
    if puzzle[center] != "A":
        return None

    if puzzle[top_left] not in ["M", "S"]:
        return None
    if puzzle[top_right] not in ["M", "S"]:
        return None
    if puzzle[bot_left] not in ["M", "S"]:
        return None
    if puzzle[bot_right] not in ["M", "S"]:
        return None

    if puzzle[top_left] == "M" and puzzle[bot_right] != "S":
        return None
    if puzzle[top_left] == "S" and puzzle[bot_right] != "M":
        return None

    if puzzle[top_right] == "M" and puzzle[bot_left] != "S":
        return None
    if puzzle[top_right] == "S" and puzzle[bot_left] != "M":
        return None

    return (center, top_left, top_right, bot_left, bot_right)


def solve_part_2(input):
    relevant_positions = []
    puzzle = Puzzle(input)
    print(puzzle)

    xmass_found = 0
    for y, puzzle_line in enumerate(puzzle.puzzle):
        for x, ch in enumerate(puzzle_line):
            if ch == "A":
                pos = Position(x, y)
                positions_found = search_v2(puzzle, pos)
                if positions_found:
                    relevant_positions.extend(positions_found)
                    xmass_found += 1
    puzzle.print_relevant_positions(relevant_positions)
    return xmass_found


if __name__ == "__main__":
    with open("./input.txt", "r") as input_fh:
        input = input_fh.read()
        output = solve_part_1(input)
        print(f"part 1: {output}")

        output = solve_part_2(input)
        print(f"part 2: {output}")
