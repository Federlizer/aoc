import unittest

from main import solve_part_1, solve_part_2


class TestSolutions(unittest.TestCase):

    def test_part_1(self):
        input = """MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX"""
        expected_output = 18
        output = solve_part_1(input)
        self.assertEqual(output, expected_output)

    def test_part_2(self):
        input = """MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX"""
        expected_output = 9
        output = solve_part_2(input)
        self.assertEqual(output, expected_output)
