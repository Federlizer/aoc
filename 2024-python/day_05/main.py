from graphlib import TopologicalSorter


class PageOrderingRule(object):

    def __init__(self, first, second):
        self.first = first
        self.second = second

    def __repr__(self):
        return f"{self.first}|{self.second}"


class Update(object):

    def __init__(self, pages):
        self.pages = pages

    def __repr__(self):
        return f"{self.pages}"

    def _get_applicable_rules(self, page_ordering_rules):
        return [
            rule for rule in page_ordering_rules
            if rule.first in self.pages and rule.second in self.pages
        ]

    def is_valid(self, page_ordering_rules):
        applicable_rules = self._get_applicable_rules(page_ordering_rules)
        for rule in applicable_rules:
            first_index = self.pages.index(rule.first)
            second_index = self.pages.index(rule.second)
            if first_index > second_index:
                return False
        return True

    def make_valid(self, page_ordering_rules):
        if self.is_valid(page_ordering_rules):
            return
        applicable_rules = self._get_applicable_rules(page_ordering_rules)

        topological_sorder = TopologicalSorter()
        for rule in applicable_rules:
            topological_sorder.add(rule.second, rule.first)

        right_order = tuple(topological_sorder.static_order())
        self.pages = list(right_order)


def parse_rule(rule_input):
    first, second = rule_input.split("|")
    return PageOrderingRule(first, second)


def parse_update(update_input):
    pages = update_input.split(",")
    return Update(pages)


def solve_part_1(input):
    page_ordering_rules = []
    updates = []

    parsing_rules = True
    for line in input.split("\n"):
        if line == "":
            if not parsing_rules:
                continue
            parsing_rules = False
            continue
        if parsing_rules:
            rule = parse_rule(line)
            page_ordering_rules.append(rule)
        else:
            update = parse_update(line)
            updates.append(update)

    valid_updates = [u for u in updates if u.is_valid(page_ordering_rules)]

    total = 0
    for update in valid_updates:
        # Find middle number
        middle_index = int((len(update.pages) - 1) / 2)
        middle_number = int(update.pages[middle_index])
        total += middle_number

    return total


def solve_part_2(input):
    page_ordering_rules = []
    updates = []

    parsing_rules = True
    for line in input.split("\n"):
        if line == "":
            parsing_rules = False
            continue
        if parsing_rules:
            rule = parse_rule(line)
            page_ordering_rules.append(rule)
        else:
            update = parse_update(line)
            updates.append(update)

    invalid_updates = [u for u in updates if not u.is_valid(page_ordering_rules)]

    for update in invalid_updates:
        update.make_valid(page_ordering_rules)

    total = 0
    for update in invalid_updates:
        middle_index = int((len(update.pages) - 1) / 2)
        middle_number = int(update.pages[middle_index])
        total += middle_number
    return total


if __name__ == "__main__":
    with open("./input.txt", "r") as input_fh:
        input = input_fh.read()
        output = solve_part_1(input)
        print(f"Part 1: {output}")
        output = solve_part_2(input)
        print(f"Part 2: {output}")
        print("Must be 4230")
