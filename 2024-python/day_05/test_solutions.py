import unittest

from main import solve_part_1, solve_part_2


class TestSolutions(unittest.TestCase):

    def test_solution_1(self):
        input = """47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47"""
        output = solve_part_1(input)
        expected_output = 143
        self.assertEqual(output, expected_output)

    def test_solution_2(self):
        input = """47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47"""
        output = solve_part_2(input)
        expected_output = 123
        self.assertEqual(output, expected_output)
